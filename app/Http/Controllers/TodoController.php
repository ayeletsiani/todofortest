<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "hello"; // שיעור ראשון מדפיסים ככה 
        //get all todos
        //$todos = Todo::all();
        //return view('todos.index',['todos' => $todos]);
            $id = Auth::id();
            if (Gate::denies('manager'))
                {
                    $boss = DB::table('employees')->where('employee',$id)->first(); // מחזיר את כל השורה בה קיים העובד 
                    $id = $boss->manager; // מתוך השורה שהתקבלה לוקח את התז של המנהל
                }
            $user = User::find($id);
            $todos = $user->todos;
            return view('todos.index', compact('todos'));
     


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) { /* אם אתה לא מנהל אז תקבל שגיאה*/
            abort(403,"Sorry you are not allowed to create todos..");
        }
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo();
        $id=Auth::id();
        $todo->title = $request->title;
        $todo->user_id = $id;
        $todo -> status = 0;

        $todo->save();
        return redirect('todos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::find($id);
        return view('todos.edit',compact('todo'));
       /* [$todo =>'todos'] */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //only if this todo belongs to user
       $todo = Todo::findOrFail($id);
       //employees are not allowed to change the title 
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit todos..");
        }   
    
        //make sure the todo belongs to the logged in user
        if(!$todo->user->id == Auth::id()) return(redirect('todos'));
   
        //test if title is dirty
       
        $todo->update($request->except(['_token']));
   
        if($request->ajax()){
            return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
        } else {          
            return redirect('todos');           
        }
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);
        $todo ->delete();
        return redirect('todos');
    }
}
