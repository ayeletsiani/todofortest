<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'sahasasar',
            'email' => 'sahar@gmaiasasal.com',
            'password' => Hash::make('12345'),
            'role' => 'employee',
            'created_at' =>date('Y-m-d G:i:s'),
            ],
            [
            
            'name' => 'yardsasaen',
            'email' => 'yarden@gmsasaail.com',
            'password' => Hash::make('12345'),
            'role' => 'employee',
            'created_at' =>date('Y-m-d G:i:s'),
            ],  
        ]);
    }
}
